<?xml version="1.0" encoding="UTF-8"?>
<process exitOnStandardFault="yes" name="ConsumeStock.bpel"
	targetNamespace="http://elass.al/ConsumeStock/"
	xmlns="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
	xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
	xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	xmlns:cs="http://elass.al/ConsumeStock/"
	xmlns:ab="http://elass.al/AccountBalance/"
	xmlns:abc="http://elass.al/AccountBalanceCallback/"
	xmlns:rm="http://elass.al/ResidentManagement/"
	xmlns:i="http://elass.al/Inventory/" >
	
	<import importType="http://schemas.xmlsoap.org/wsdl/"
		location="ConsumeStock.wsdl" namespace="http://elass.al/ConsumeStock/" />
	<import importType="http://schemas.xmlsoap.org/wsdl/"
		location="AccountBalance.wsdl" namespace="http://elass.al/AccountBalance/" />
	<import importType="http://schemas.xmlsoap.org/wsdl/"
		location="AccountBalanceCallback.wsdl" namespace="http://elass.al/AccountBalanceCallback/" />
	<import importType="http://schemas.xmlsoap.org/wsdl/"
		location="ResidentManagement.wsdl" namespace="http://elass.al/ResidentManagement/" />
	<import importType="http://schemas.xmlsoap.org/wsdl/"
		location="Inventory.wsdl" namespace="http://elass.al/Inventory/" />
		
	<correlationSets>
		<correlationSet name="TransID" properties="cs:TransID"/>
	</correlationSets>
	
	<partnerLinks>
		<partnerLink name="resident"
			partnerLinkType="cs:ConsumeStockPLT"
			myRole="ConsumeStockService" />
		<partnerLink name="accountManager"
			partnerLinkType="cs:AccountBalancePLT"
			partnerRole="accountManager"
			myRole="accountManagerCallback" />
		<partnerLink name="residentManager"
			partnerLinkType="cs:ResidentManagementPLT"
			partnerRole="residentManager" />
		<partnerLink name="inventoryManager"
			partnerLinkType="i:InventoryPLT"
			partnerRole="inventoryManager" />
	</partnerLinks>
	
	<variables>
		<variable name="entity" type="xsd:string" />
		<variable name="item" type="xsd:string" />
		<variable name="amount" type="xsd:int" />
		<variable messageType="cs:ConsumeStockRequest" name="consumeStockRequest" />
		<variable messageType="cs:ConsumeStockResponse" name="main" />
		<variable messageType="ab:IsEntityRequest" name="isEntityRequest" />
		<variable messageType="ab:IsEntityResponse" name="isEntityResponse" />
		<variable messageType="ab:GetBalanceRequest" name="getBalanceRequest" />
		<variable messageType="ab:GetBalanceResponse" name="getBalanceResponse" />
		<variable messageType="rm:IsResidentRequest" name="isResidentRequest" />
		<variable messageType="rm:IsResidentResponse" name="isResidentResponse" />
		<variable messageType="i:GetInventoryItemRequest" name="getInventoryItemRequest" />
		<variable messageType="i:GetInventoryItemResponse" name="getInventoryItemResponse" />
		<variable messageType="i:UpdateStockRequest" name="updateStockRequest" />
		<variable messageType="i:UpdateStockResponse" name="updateStockResponse" />
		<variable messageType="ab:AddMutationRequest" name="addMutationRequest" />
		<variable messageType="ab:AddMutationResponse" name="addMutationResponse" />
		<variable messageType="ab:VerifyBalancesRequest" name="verifyBalancesRequest" />
		<variable messageType="abc:receiveBalancesVerificationCallbackRequest" name="verifyBalancesResult" />
	</variables>
	
	<sequence name="MainSequence">
        <receive name="ReceiveMainRequest" 
        	partnerLink="resident"
        	operation="ConsumeStock" 
        	variable="consumeStockRequest" 
        	createInstance="yes" />
       	
        	
        	
        <assign>
        	<!-- Store resident. -->
        	<copy>
        		<from>$consumeStockRequest.Details/Resident</from>
        		<to>$entity</to>
        	</copy>
        	<copy>
        		<from>$consumeStockRequest.Details/Item</from>
        		<to>$item</to>
        	</copy>
        	<copy>
        		<from>$consumeStockRequest.Details/Amount</from>
        		<to>$amount</to>
       		</copy>
        	
        	<!-- Prepare verification and GetBalance requests. -->
	  		<copy>
  				<from>$entity</from>
  				<to>$isEntityRequest.Entity</to>
  			</copy>
  			<copy>
  				<from>$entity</from>
  				<to>$getBalanceRequest.Entity</to>
  			</copy>
  			<copy>
  				<from>$entity</from>
  				<to>$isResidentRequest.ResidentName</to>
  			</copy>
  			
  			<!-- Prepare get inventory item request. -->
  			<copy>
  				<from>$item</from>
  				<to>$getInventoryItemRequest.ItemName</to>
  			</copy>
  			
  			<!-- Prepare stock update request. -->
  			<copy>
  				<from>
  					<literal>
	  					<i:StockUpdate xmlns="">
	  						<ItemName /><RelativeAmount /><NewPrice />
	  					</i:StockUpdate>
  					</literal>
  				</from>
  				<to>$updateStockRequest.StockUpdate</to>
  			</copy>
  			<copy>
  				<from>$item</from>
  				<to>$updateStockRequest.StockUpdate//ItemName</to>
  			</copy>
  			<copy>
  				<from>-1 * $amount</from>
  				<to>$updateStockRequest.StockUpdate//RelativeAmount</to>
  			</copy>
       	</assign>
       	
  		
  		<flow>
  			<!-- Check if the inventory has sufficient stock of the item, and get the item price. -->
  			<sequence>
  				<invoke operation="GetInventoryItem"
  					partnerLink="inventoryManager"
  					inputVariable="getInventoryItemRequest"
  					outputVariable="getInventoryItemResponse" />
 				<if>
 					<condition>$getInventoryItemResponse.Item/Stock - $amount &lt; 0</condition>
					<sequence>
						<assign>
							<copy>
								<from>
									<literal>
										<cs:ConsumeStockResponse xmlns="">
											<Error>The stock of the item is too low.</Error>
										</cs:ConsumeStockResponse>
									</literal>
								</from>
								<to>$main.Response</to>
							</copy>
						</assign>
						<reply name="Reply" 
				        	partnerLink="resident"
				        	operation="ConsumeStock" 
				        	variable="main" />
			        	<exit />
					</sequence>
 				</if>
  			</sequence>
  		
  			<!-- Check if the resident exists as an entity in the account balance service. -->
  			<sequence>
				<invoke operation="IsEntity" 
					partnerLink="accountManager" 
					inputVariable="isEntityRequest" 
					outputVariable="isEntityResponse"/>
				<if>
					<condition>$isEntityResponse.IsEntity = false()</condition>
					<sequence>
						<assign>
							<copy>
								<from>
									<literal>
										<cs:ConsumeStockResponse xmlns="">
											<Error>Entity was not found in the account balance manager.</Error>
										</cs:ConsumeStockResponse>
									</literal>
								</from>
								<to>$main.Response</to>
							</copy>
						</assign>
						<reply name="Reply" 
				        	partnerLink="resident"
				        	operation="ConsumeStock" 
				        	variable="main" />
			        	<exit />
					</sequence>
				</if>
				<!-- Get the resident's current account balance. -->
				<invoke operation="GetBalance"
					partnerLink="accountManager"
					inputVariable="getBalanceRequest"
					outputVariable="getBalanceResponse"/>
			</sequence>
			
			<!-- Check if the resident in the resident management service. -->
			<sequence>
				<invoke operation="IsResident" 
					partnerLink="residentManager" 
					inputVariable="isResidentRequest" 
					outputVariable="isResidentResponse"/>
				<if>
					<condition>$isResidentResponse.IsResident = false()</condition>
					<sequence>
						<assign>
							<copy>
								<from>
									<literal>
										<cs:ConsumeStockResponse xmlns="">
											<Error>Resident was not found in the resident manager.</Error>
										</cs:ConsumeStockResponse>
									</literal>
								</from>
								<to>$main.Response</to>
							</copy>
						</assign>
						<reply name="Reply" 
				        	partnerLink="resident"
				        	operation="ConsumeStock" 
				        	variable="main" />
			        	<exit />
					</sequence>
				</if>
			</sequence>
		</flow>
		
		<!-- Check if the resident's account balance is sufficient. -->
		<if>
			<condition>$getBalanceResponse.Balance/Value - $amount * $getInventoryItemResponse.Item/Price &lt; -100</condition>
			<sequence>
				<assign>
					<copy>
						<from>
							<literal>
								<cs:ConsumeStockResponse xmlns="">
									<Error>The resident's account balance is too low.</Error>
								</cs:ConsumeStockResponse>
							</literal>
						</from>
						<to>$main.Response</to>
					</copy>
				</assign>
				<reply name="Reply" 
		        	partnerLink="resident"
		        	operation="ConsumeStock" 
		        	variable="main" />
	        	<exit />
			</sequence>
		</if>
		
		<!-- Commence the updating! -->
		<flow>
			<!-- Update the inventory. -->
			<sequence>
				<invoke operation="UpdateStock"
					partnerLink="inventoryManager"
					inputVariable="updateStockRequest"
					outputVariable="updateStockResponse" />
			</sequence>
			
			<!-- Update the account balance and (async) verify balances. -->
			<sequence>
				<assign>
		  			<!--  Prepare account balance update request. -->
		  			<copy>
			  			<from>
			  				<literal>
			  					<ab:MutationEntry xmlns="">
			  						<MutatedBy /><Value /><Description /><Shares />
			  					</ab:MutationEntry>
			  				</literal>
			  			</from>
			  			<to>$addMutationRequest.Entry</to>
		  			</copy>
		  			<copy>
		  				<from>$entity</from>
		  				<to>$addMutationRequest.Entry//MutatedBy</to>
		  			</copy>
		  			<copy>
						<from>-1.0 * $amount * $getInventoryItemResponse.Item/Price</from>
						<to>$addMutationRequest.Entry//Value</to>
					</copy>
					<copy>
						<from>concat('Consumed ', $amount, ' times ', $item, ' from the inventory')</from>
						<to>$addMutationRequest.Entry//Description</to>
					</copy>
				</assign>
				<invoke operation="AddMutation" 
					partnerLink="accountManager" 
					inputVariable="addMutationRequest" 
					outputVariable="addMutationResponse">
					<correlations>
						<correlation set="TransID" initiate="yes" pattern="response" />
					</correlations>
				</invoke>
				
				<!-- Verify the balances. -->
				<assign>
					<!-- Get the correlation value (the mutation timestamp) and put it in the next request. -->
					<copy>
						<from>$addMutationResponse.Balance/LastMutationDate</from>
						<to>$verifyBalancesRequest.TransID</to>
					</copy>
				</assign>
				<invoke operation="VerifyBalances"
					partnerLink="accountManager" 
					portType="ab:AccountBalancePT" 
					inputVariable="verifyBalancesRequest" />
			</sequence>
		</flow>
		
		
		
		<!-- Receive the callback with the account balance verification result. -->
		<receive name="receive_verificationResult" 
			partnerLink="accountManager"
			portType="abc:VerifyBalancesCallbackPT"
			operation="ReceiveVerificationResult"
			variable="verifyBalancesResult">
			<correlations>
				<correlation set="TransID" initiate="no" />
			</correlations>
		</receive>
		<if>
			<condition>$verifyBalancesResult.Verification/Result = 'false'</condition>
			<!-- Balances are inconsistent! Help! -->
				<assign>
					<copy>
						<from>
							<literal>
								<cs:ConsumeStockResponse xmlns="">
									<Success>
		        						<Resident><Name /><NewBalance /></Resident>
		        						<InventoryItem><Item /><NewStock /><PaidItemPrice /></InventoryItem>
       								</Success>
									<Error>Account balance consistency verification failed! Contact your system administrator.</Error>
								</cs:ConsumeStockResponse>
							</literal>
						</from>
						<to>$main.Response</to>
					</copy>
				</assign>
			<else>
				<!-- Balances are consistent, format the successful response. -->
				<assign>
					<copy>
						<from>
							<literal>
								<cs:ConsumeStockResponse xmlns="">
									<Success>
		        						<Resident><Name /><NewBalance /></Resident>
		        						<InventoryItem><Item /><NewStock /><PaidItemPrice /></InventoryItem>
       								</Success>
								</cs:ConsumeStockResponse>
							</literal>
						</from>
						<to>$main.Response</to>
					</copy>
				</assign>
			</else>
		</if>
		
		<assign>
			<!-- Collect all results and put them in the final response. -->
        	<copy>
        		<from>$entity</from>
        		<to>$main.Response//Name</to>
        	</copy>
			<copy>
				<from>$addMutationResponse.Balance/Value</from>
				<to>$main.Response//NewBalance</to>
			</copy>
			<copy>
				<from>$updateStockResponse.Item/Name</from>
				<to>$main.Response//Item</to>
			</copy>
			<copy>
				<from>$updateStockResponse.Item/Stock</from>
				<to>$main.Response//NewStock</to>
			</copy>
			<copy>
				<from>$updateStockResponse.Item/Price</from>
				<to>$main.Response//PaidItemPrice</to>
			</copy>
		</assign>
  		
        <reply name="Reply" 
        	partnerLink="resident"
        	operation="ConsumeStock" 
        	variable="main"/>
    </sequence>
</process>
